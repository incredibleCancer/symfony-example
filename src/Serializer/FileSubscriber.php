<?php

namespace App\Serializer;

use App\Entity\File;
use App\Services\LinkGeneratorService;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;

class FileSubscriber implements EventSubscriberInterface
{
    /** @var LinkGeneratorService */
    protected $linkGeneratorService;

    /**
     * FileEventSubscriber constructor.
     * @param LinkGeneratorService $linkGeneratorService
     */
    public function __construct(LinkGeneratorService $linkGeneratorService)
    {
        $this->linkGeneratorService = $linkGeneratorService;
    }

    public static function getSubscribedEvents()
    {
        return [[
            'event' => Events::POST_SERIALIZE,
            'method' => 'onPostSerialize',
            'class' => File::class,
            'format' => 'json'
        ]];
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();
        $visitor->setData('url', $this->linkGeneratorService->generateImageUrl($event->getObject()));
    }
}
