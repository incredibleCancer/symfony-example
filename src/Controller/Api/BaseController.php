<?php declare(strict_types=1);

namespace App\Controller\Api;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use BunBundle\Request\Form\FormValidationException;
use BunBundle\Response\ApiJsonResponse;

abstract class BaseController extends \Tapcore\ApiBundle\Controller\BaseApiController
{
    const JSON_DEFAULT_OPTIONS = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE;

    protected function createSuccessApiJsonResponse($data = null, $status = Response::HTTP_OK): ApiJsonResponse
    {
        return parent::createSuccessApiJsonResponse($data, $status)
            ->setEncodingOptions(self::JSON_DEFAULT_OPTIONS);
    }

    protected function createErrorApiJsonResponse($errorMessage, $status = Response::HTTP_INTERNAL_SERVER_ERROR): ApiJsonResponse
    {
        return parent::createErrorApiJsonResponse($errorMessage, $status)
            ->setEncodingOptions(self::JSON_DEFAULT_OPTIONS);
    }

    protected function createFormValidationException(FormInterface $form): FormValidationException
    {
        return new FormValidationException($form->getErrors(true));
    }
}
