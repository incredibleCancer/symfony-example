<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Client;
use App\Entity\Client\Relation;
use App\Forms\Client\RelationType;
use App\Forms\ClientType;
use App\Request\Filters\ClientFilter;
use App\Request\Filters\PaymentFilter;
use App\Request\Filters\RequestFilter;
use App\Services\Client\PaymentService;
use App\Services\Client\RelationService;
use App\Services\RequestService;
use App\Services\ClientService;
use App\Services\FileUploadService;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use BunBundle\Configuration\Query;
use BunBundle\Configuration\Route\Version;
use BunBundle\Configuration\Serialization\SerializationGroupsParam;

class ClientController extends BaseController
{
    /** @var ClientService */
    protected $clientService;

    /** @var PaymentService */
    protected $paymentService;

    /** @var FileUploadService */
    protected $fileUploadService;

    /** @var RequestService */
    protected $requestService;

    /** @var RelationService */
    protected $relationService;

    public function __construct(
        ClientService $clientService,
        PaymentService $paymentService,
        FileUploadService $fileUploadService,
        RequestService $requestService,
        RelationService $relationService
    ) {
        $this->clientService = $clientService;
        $this->paymentService = $paymentService;
        $this->fileUploadService = $fileUploadService;
        $this->requestService = $requestService;
        $this->relationService = $relationService;
    }

    /**
     * @Route(path="/clients", methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("city", mapping={"city_id"}, required=false, convertTo="App\Entity\City")
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param ClientFilter $filter
     *
     * @return Response
     */
    public function listAction(ClientFilter $filter): Response
    {
        $clients = $this->clientService->getClientsByFilter($filter);

        return $this->createSuccessApiJsonResponse($clients);
    }

    /**
     * @Route(path="/clients", methods={"POST"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function createAction(Request $request): Response
    {
        $client = new Client();

        $form = $this->createForm(ClientType::class, $client);
        $form->submit($request->request->all(), false);

        /** @var File $photoFile */
        $photoFile = $form->get('file_to_upload')->getData();

        if (null !== $photoFile) {
            $logo = $this->fileUploadService->saveFromFile($photoFile, '/client');
            $client->setPhoto($logo);
        }

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->clientService->save($client);


        return $this->createSuccessApiJsonResponse($client, Response::HTTP_CREATED);
    }

    /**
     * @Route(path="/clients/{id}", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     *
     * @return Response
     */
    public function getAction(int $id): Response
    {
        $hotel = $this->clientService->getClient($id);

        if (null === $hotel) {
            throw $this->createNotFoundException(sprintf('Client #%d does not exist', $id));
        }

        return $this->createSuccessApiJsonResponse($hotel);
    }

    /**
     * @Route(path="/clients/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function editAction(int $id, Request $request): Response
    {
        $client = $this->clientService->getClient($id);

        if (null === $client) {
            throw $this->createNotFoundException(sprintf('Client #%d does not exist', $id));
        }

        $form = $this->createForm(ClientType::class, $client);
        $form->submit($request->request->all(), false);

        /** @var File $photoFile */
        $photoFile = $form->get('file_to_upload')->getData();

        if (null !== $photoFile) {
            $logo = $this->fileUploadService->saveFromFile($photoFile, '/client');
            $client->setPhoto($logo);
        }

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->clientService->save($client);

        return $this->createSuccessApiJsonResponse($client);
    }

    /**
     * @Route(path="/clients/{id}", requirements={"id": "\d+"}, methods={"DELETE"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     *
     * @return Response
     * @throws ORMException
     */
    public function deleteAction(int $id): Response
    {
        $client = $this->clientService->getClient($id);

        if (null === $client) {
            throw $this->createNotFoundException(sprintf('Client #%d does not exist', $id));
        }

        $this->clientService->remove($client);

        return $this->createSuccessApiJsonResponse([
            'success' => true,
        ]);
    }

    /**
     * @Route(path="/clients/{id}/payments", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("client", mapping={"id"}, convertTo="App\Entity\Client", required=true)
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param PaymentFilter $filter
     * @return Response
     */
    public function paymentsAction(PaymentFilter $filter): Response
    {
        $seasons = $this->paymentService->getPaymentsByFilter($filter);

        return $this->createSuccessApiJsonResponse($seasons);
    }

    /**
     * @Route(path="/clients/{id}/requests", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("client", mapping={"id"}, convertTo="App\Entity\Client", required=true)
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param RequestFilter $filter
     * @return Response
     */
    public function requestsAction(RequestFilter $filter): Response
    {
        $seasons = $this->requestService->getRequestsByFilter($filter);

        return $this->createSuccessApiJsonResponse($seasons);
    }

    /**
     * @Route(path="/clients/{id}/passport", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("client", mapping={"id"}, convertTo="App\Entity\Client", required=true)
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param RequestFilter $filter
     * @return Response
     */
    public function passportAction(RequestFilter $filter): Response
    {
        $passport = $filter->getClient()->getPassport();

        return $this->createSuccessApiJsonResponse($passport);
    }

    /**
     * @Route(path="/clients/{id}/foreign-passport", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("client", mapping={"id"}, convertTo="App\Entity\Client", required=true)
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param RequestFilter $filter
     * @return Response
     */
    public function foreignPassportAction(RequestFilter $filter): Response
    {
        $passport = $filter->getClient()->getForeignPassport();

        return $this->createSuccessApiJsonResponse($passport);
    }

    /**
     * @Route(path="/clients/{id}/related-clients", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     * @return Response
     */
    public function relatedClientsAction(int $id): Response
    {
        $client = $this->clientService->getClient($id);

        return $this->createSuccessApiJsonResponse($client->getRelatedClients());
    }

    /**
     * @Route(path="/clients/{clientId}/relations/{id}", requirements={"clientId": "\d+", "id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("client", convertTo="App\Entity\Client", mapping={"clientId"})
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param Client $client
     * @param int $id
     * @return Response
     */
    public function getRelationAction(Client $client, int $id): Response
    {
        $relation = $this->relationService->getRelation($client->getId(), $id);

        return $this->createSuccessApiJsonResponse($relation);
    }

    /**
     * @Route(path="/clients/{clientId}/relations", requirements={"clientId": "\d+"}, methods={"POST"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("client", convertTo="App\Entity\Client", mapping={"clientId"})
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param Client $client
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function createRelationAction(Client $client, Request $request): Response
    {
        $relation = new Relation();

        $form = $this->createForm(RelationType::class, $relation);
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->relationService->save($relation);

        return $this->createSuccessApiJsonResponse($relation, Response::HTTP_CREATED);
    }

    /**
     * @Route(path="/clients/{clientId}/relations/{id}", requirements={"clientId": "\d+", "id": "\d+"}, methods={"PUT"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("client", convertTo="App\Entity\Client", mapping={"clientId"})
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param Client $client
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws ORMException
     */
    public function editRelationAction(Client $client, int $id, Request $request): Response
    {
        $relation = $this->relationService->getRelation($client->getId(), $id);

        if (null === $relation) {
            throw $this->createNotFoundException(sprintf('Relation #%d does not exist', $id));
        }

        $form = $this->createForm(RelationType::class, $relation);
        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->relationService->save($relation);

        return $this->createSuccessApiJsonResponse($relation);
    }
}
