<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Partner;
use App\Forms\PartnerType;
use App\Request\Filters\PartnerFilter;
use App\Services\FileUploadService;
use App\Services\PartnerService;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use BunBundle\Configuration\Query;
use BunBundle\Configuration\Route\Version;
use BunBundle\Configuration\Serialization\SerializationGroupsParam;

class PartnerController extends BaseController
{
    /** @var PartnerService */
    protected $partnerService;

    /** @var FileUploadService */
    protected $fileUploadService;

    public function __construct(PartnerService $partnerService, FileUploadService $fileUploadService)
    {
        $this->partnerService = $partnerService;
        $this->fileUploadService = $fileUploadService;
    }

    /**
     * @Route(path="/partners", methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("country", mapping={"country_id"}, required=false, convertTo="App\Entity\Country")
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param PartnerFilter $filter
     *
     * @return Response
     */
    public function listAction(PartnerFilter $filter): Response
    {
        $countries = $this->partnerService->getPartnersByFilter($filter);

        return $this->createSuccessApiJsonResponse($countries);
    }

    /**
     * @Route(path="/partners/{id}", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     *
     * @return Response
     */
    public function getAction(int $id): Response
    {
        $partner = $this->partnerService->getPartner($id);

        if (null === $partner) {
            throw $this->createNotFoundException(sprintf('Partner #%d does not exist', $id));
        }

        return $this->createSuccessApiJsonResponse($partner);
    }

    /**
     * @Route(path="/partners", methods={"POST"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function createAction(Request $request): Response
    {
        $partner = new Partner();

        $form = $this->createForm(PartnerType::class, $partner);
        $form->submit($request->request->all());

        /** @var File $photoFile */
        $photoFile = $form->get('file_to_upload')->getData();

        if (null !== $photoFile) {
            $logo = $this->fileUploadService->saveFromFile($photoFile, '/partner');
            $partner->setFile($logo);
        }

        $countries = $form->get('country_ids')->getData();
        $partner->setCountries($countries);

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->partnerService->save($partner);

        return $this->createSuccessApiJsonResponse($partner, Response::HTTP_CREATED);
    }

    /**
     * @Route(path="/partners/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param int $id
     * @param Request $request
     * @return Response
     * @throws ORMException
     */
    public function editAction(int $id, Request $request): Response
    {
        if (null === $partner = $this->partnerService->getPartner($id)) {
            throw $this->createNotFoundException(sprintf('Partner #%d does not exist', $id));
        }

        $form = $this->createForm(PartnerType::class, $partner);
        $form->submit($request->request->all());

        /** @var File $photoFile */
        $photoFile = $form->get('file_to_upload')->getData();

        if (null !== $photoFile) {
            $logo = $this->fileUploadService->saveFromFile($photoFile, '/partner');
            $partner->setFile($logo);
        }

        $countries = $form->get('country_ids')->getData();
        $partner->setCountries($countries);

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->partnerService->save($partner);

        return $this->createSuccessApiJsonResponse($partner);
    }
}
