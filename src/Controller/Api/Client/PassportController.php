<?php declare(strict_types=1);

namespace App\Controller\Api\Client;

use App\Controller\Api\BaseController;
use App\Entity\Client\Passport;
use App\Forms\Client\PassportType;
use App\Services\Client\PassportService;
use App\Services\ClientService;
use App\Services\FileUploadService;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use BunBundle\Configuration\Query;
use BunBundle\Configuration\Route\Version;
use BunBundle\Configuration\Serialization\SerializationGroupsParam;

class PassportController extends BaseController
{
    /** @var ClientService */
    protected $clientService;

    /** @var PassportService */
    protected $passportService;

    /** @var FileUploadService */
    protected $fileUploadService;

    public function __construct(
        ClientService $clientService,
        PassportService $passportService,
        FileUploadService $fileUploadService
    ) {
        $this->clientService = $clientService;
        $this->passportService = $passportService;
        $this->fileUploadService = $fileUploadService;
    }

    /**
     * @Route(path="/passports", methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @return Response
     */
    public function listAction(): Response
    {
        $clients = $this->passportService->getAll();

        return $this->createSuccessApiJsonResponse($clients);
    }

    /**
     * @Route(path="/passports", methods={"POST"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function createAction(Request $request): Response
    {
        $passport = new Passport();

        $form = $this->createForm(PassportType::class, $passport);
        $form->submit($request->request->all(), false);

        /** @var File $scan1 */
        $scan1 = $form->get('file1')->getData();
        /** @var File $scan2 */
        $scan2 = $form->get('file2')->getData();

        if (null !== $scan1) {
            $scan = $this->fileUploadService->saveFromFile($scan1, '/client_passport');
            $passport->setScan1($scan);
        }

        if (null !== $scan2) {
            $scan = $this->fileUploadService->saveFromFile($scan2, '/client_passport');
            $passport->setScan2($scan);
        }

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->passportService->save($passport);


        return $this->createSuccessApiJsonResponse($passport, Response::HTTP_CREATED);
    }

    /**
     * @Route(path="/passports/{id}", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     *
     * @return Response
     */
    public function getAction(int $id): Response
    {
        $hotel = $this->passportService->getPassport($id);

        if (null === $hotel) {
            throw $this->createNotFoundException(sprintf('Passport #%d does not exist', $id));
        }

        return $this->createSuccessApiJsonResponse($hotel);
    }

    /**
     * @Route(path="/passports/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function editAction(int $id, Request $request): Response
    {
        $passport = $this->passportService->getPassport($id);

        if (null === $passport) {
            throw $this->createNotFoundException(sprintf('Passport #%d does not exist', $id));
        }

        $form = $this->createForm(PassportType::class, $passport);
        $form->submit($request->request->all(), false);

        /** @var File $scan1 */
        $scan1 = $form->get('file1')->getData();
        /** @var File $scan2 */
        $scan2 = $form->get('file2')->getData();

        if (null !== $scan1) {
            $scan = $this->fileUploadService->saveFromFile($scan1, '/client_passport');
            $passport->setScan1($scan);
        }

        if (null !== $scan2) {
            $scan = $this->fileUploadService->saveFromFile($scan2, '/client_passport');
            $passport->setScan2($scan);
        }

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->passportService->save($passport);

        return $this->createSuccessApiJsonResponse($passport);
    }

    /**
     * @Route(path="/passports/{id}", requirements={"id": "\d+"}, methods={"DELETE"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     *
     * @return Response
     * @throws ORMException
     */
    public function deleteAction(int $id): Response
    {
        $client = $this->passportService->getPassport($id);

        if (null === $client) {
            throw $this->createNotFoundException(sprintf('Passport #%d does not exist', $id));
        }

        $this->passportService->remove($client);

        return $this->createSuccessApiJsonResponse([
            'success' => true,
        ]);
    }
}
