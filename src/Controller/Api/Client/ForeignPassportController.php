<?php declare(strict_types=1);

namespace App\Controller\Api\Client;

use App\Controller\Api\BaseController;
use App\Entity\Client\ForeignPassport;
use App\Forms\Client\ForeignPassportType;
use App\Services\Client\ForeignPassportService;
use App\Services\RequestService;
use App\Services\ClientService;
use App\Services\FileUploadService;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use BunBundle\Configuration\Query;
use BunBundle\Configuration\Route\Version;
use BunBundle\Configuration\Serialization\SerializationGroupsParam;

class ForeignPassportController extends BaseController
{
    /** @var ClientService */
    protected $clientService;

    /** @var ForeignPassportService */
    protected $foreignPassportService;

    /** @var FileUploadService */
    protected $fileUploadService;

    /** @var RequestService */
    protected $requestService;

    public function __construct(
        ClientService $clientService,
        ForeignPassportService $foreignPassportService,
        FileUploadService $fileUploadService
    ) {
        $this->clientService = $clientService;
        $this->foreignPassportService = $foreignPassportService;
        $this->fileUploadService = $fileUploadService;
    }

    /**
     * @Route(path="/foreign-passports", methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @return Response
     */
    public function listAction(): Response
    {
        $foreignPassports = $this->foreignPassportService->getAll();

        return $this->createSuccessApiJsonResponse($foreignPassports);
    }

    /**
     * @Route(path="/foreign-passports", methods={"POST"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function createAction(Request $request): Response
    {
        $foreignPassport = new ForeignPassport();

        $form = $this->createForm(ForeignPassportType::class, $foreignPassport);
        $form->submit($request->request->all(), false);

        /** @var File $scan */
        $scan = $form->get('file')->getData();

        if (null !== $scan) {
            $scan = $this->fileUploadService->saveFromFile($scan, '/client_foreign_passport');
            $foreignPassport->setScan($scan);
        }

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->foreignPassportService->save($foreignPassport);


        return $this->createSuccessApiJsonResponse($foreignPassport, Response::HTTP_CREATED);
    }

    /**
     * @Route(path="/foreign-passports/{id}", requirements={"id": "\d+"}, methods={"GET"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     *
     * @return Response
     */
    public function getAction(int $id): Response
    {
        $foreignPassport = $this->foreignPassportService->getForeignPassport($id);

        if (null === $foreignPassport) {
            throw $this->createNotFoundException(sprintf('Foreign passport #%d does not exist', $id));
        }

        return $this->createSuccessApiJsonResponse($foreignPassport);
    }

    /**
     * @Route(path="/foreign-passports/{id}", requirements={"id": "\d+"}, methods={"PUT"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     * @param Request $request
     *
     * @return Response
     * @throws ORMException
     */
    public function editAction(int $id, Request $request): Response
    {
        $foreignPassport = $this->foreignPassportService->getForeignPassport($id);

        if (null === $foreignPassport) {
            throw $this->createNotFoundException(sprintf('Foreign passport #%d does not exist', $id));
        }

        $form = $this->createForm(ForeignPassportType::class, $foreignPassport);
        $form->submit($request->request->all(), false);

        /** @var File $scan */
        $scan = $form->get('file')->getData();

        if (null !== $scan) {
            $scan = $this->fileUploadService->saveFromFile($scan, '/client_foreign_passport');
            $foreignPassport->setScan($scan);
        }

        if (!$form->isValid()) {
            throw $this->createFormValidationException($form);
        }

        $this->foreignPassportService->save($foreignPassport);

        return $this->createSuccessApiJsonResponse($foreignPassport);
    }

    /**
     * @Route(path="/foreign-passports/{id}", requirements={"id": "\d+"}, methods={"DELETE"})
     * @Version(from="1.0", to="1.0", useInSerialization=true)
     *
     * @Query\IntegerParam("id")
     * @SerializationGroupsParam(name="fields", includeDefault=true)
     *
     * @param int $id
     *
     * @return Response
     * @throws ORMException
     */
    public function deleteAction(int $id): Response
    {
        $foreignPassport = $this->foreignPassportService->getForeignPassport($id);

        if (null === $foreignPassport) {
            throw $this->createNotFoundException(sprintf('Foreign passport #%d does not exist', $id));
        }

        $this->foreignPassportService->remove($foreignPassport);

        return $this->createSuccessApiJsonResponse([
            'success' => true,
        ]);
    }
}

