<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route(path="/", name="tss.homepage")
     * @Route("/{url}", name="tss.front_application", requirements={"url": "(?!api|img/)(.*|)"})
     */
    public function indexAction()
    {
        return $this->render('default/homepage.html.twig');
    }
}
