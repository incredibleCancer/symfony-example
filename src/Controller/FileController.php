<?php declare(strict_types=1);

namespace App\Controller;

use App\Services\FileService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class FileController extends Controller
{
    /** @var FileService */
    protected $fileService;

    /** @var string */
    public $storage;

    /**
     * DefaultController constructor.
     * @param FileService $fileService
     * @param string $storage
     */
    public function __construct(
        FileService $fileService,
        string $storage
    ) {
        $this->fileService = $fileService;
        $this->storage = $storage;
    }

    /**
     * @Route(path="/img/{storagePath}/{hash}-{size}.{ext}", name="tss.image")
     * @param string $hash
     * @return Response
     */
    public function imageAction(string $hash)
    {
        if (null === $file = $this->fileService->getFileByHash($hash)) {
            throw new NotFoundHttpException();
        }

        $fullFilename = "{$this->storage}{$file->getStoragePath()}/{$file->getHash()}.{$file->getExtension()}";
        $imageContent = file_get_contents($fullFilename);

        $headers = [
            'Content-Type' => mime_content_type($fullFilename),
            'Content-Length' => strlen($imageContent),
            'Content-Disposition' =>  "inline; filename=\"{$file->getHash()}.{$file->getExtension()}\""
        ];

        return new Response($imageContent, 200, $headers);
    }
}
