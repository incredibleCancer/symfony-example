<?php

namespace App\Services;

use App\Entity\File;
use App\Repository\FileRepository;

class FileService
{
    /** @var FileRepository */
    protected $repository;

    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param File $file
     *
     * @return $this
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(File $file): self
    {
        $this->repository->save($file);

        return $this;
    }

    /**
     * @param File $file
     *
     * @return $this
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(File $file): self
    {
        $this->repository->remove($file);

        return $this;
    }

    public function getFile(int $id): ?File
    {
        return $this->repository->find($id);
    }

    public function getFileByHash(string $hash): ?File
    {
        return $this->repository->findOneBy([
            'hash' => $hash,
        ]);
    }
}
