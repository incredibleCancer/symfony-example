<?php declare(strict_types=1);

namespace App\Services;

use App\Entity\Client;
use App\Repository\ClientRepository;
use App\Request\Filters\ClientFilter;
use Doctrine\ORM\ORMException;

class ClientService
{
    /** @var ClientRepository */
    protected $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllClients(): array
    {
        return $this->repository
            ->findAll();
    }

    /**
     * @param Client $client
     *
     * @return $this
     * @throws ORMException
     */
    public function save(Client $client): self
    {
        $this->repository->save($client);

        return $this;
    }

    /**
     * @param Client $client
     *
     * @return $this
     * @throws ORMException
     */
    public function remove(Client $client): self
    {
        $this->repository->remove($client);

        return $this;
    }

    public function getClient(int $id): ?Client
    {
        return $this->repository->find($id);
    }

    public function getClientsByFilter(ClientFilter $filter): array
    {
        return $this->repository
            ->findByFilter($filter);
    }
}
