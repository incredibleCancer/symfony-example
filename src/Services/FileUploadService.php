<?php

namespace App\Services;

use App\Repository\FileRepository;
use Symfony\Component\HttpFoundation\File\File;

class FileUploadService
{
    /** @var string */
    protected $storage;

    /** @var FileRepository */
    protected $repository;

    /**
     * @param FileRepository $repository
     * @param $storage
     */
    public function __construct(FileRepository $repository, $storage)
    {
        $this->repository = $repository;
        $this->storage = $storage;
    }

    /**
     * @param File $uploadedFile
     * @param string $directory
     * @return \App\Entity\File
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveFromFile(File $uploadedFile, $directory = '/')
    {
        $file = new \App\Entity\File();
        $file->setExtension($uploadedFile->getExtension());
        $file->setHash($this->generateHash($uploadedFile));
        $file->setName($uploadedFile->getFilename());
        $file->setSize($uploadedFile->getSize());
        $file->setStoragePath($directory);

        $uploadedFile->move("{$this->storage}{$directory}", "{$file->getHash()}.{$file->getExtension()}");

        $this->repository->save($file);

        return $file;
    }

    protected function generateHash(File $uploadedFile)
    {
        return md5(md5_file($uploadedFile->getPathname()) . microtime(true));
    }
}
