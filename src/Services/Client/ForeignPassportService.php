<?php

namespace App\Services\Client;

use App\Entity\Client\ForeignPassport;
use App\Repository\Client\ForeignPassportRepository;

class ForeignPassportService
{
    /** @var ForeignPassportRepository */
    protected $repository;

    public function __construct(ForeignPassportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll(): array
    {
        return $this->repository
            ->findAll();
    }

    /**
     * @param ForeignPassport $passport
     *
     * @return $this
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(ForeignPassport $passport): self
    {
        $this->repository->save($passport);

        return $this;
    }

    /**
     * @param ForeignPassport $passport
     *
     * @return $this
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(ForeignPassport $passport): self
    {
        $this->repository->remove($passport);

        return $this;
    }

    public function getForeignPassport(int $id): ?ForeignPassport
    {
        return $this->repository->find($id);
    }
}
