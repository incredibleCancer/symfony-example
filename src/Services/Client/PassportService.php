<?php

namespace App\Services\Client;

use App\Entity\Client\Passport;
use App\Repository\Client\PassportRepository;

class PassportService
{
    /** @var PassportRepository */
    protected $repository;

    public function __construct(PassportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll(): array
    {
        return $this->repository
            ->findAll();
    }

    /**
     * @param Passport $passport
     *
     * @return $this
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(Passport $passport): self
    {
        $this->repository->save($passport);

        return $this;
    }

    /**
     * @param Passport $passport
     *
     * @return $this
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(Passport $passport): self
    {
        $this->repository->remove($passport);

        return $this;
    }

    public function getPassport(int $id): ?Passport
    {
        return $this->repository->find($id);
    }
}
