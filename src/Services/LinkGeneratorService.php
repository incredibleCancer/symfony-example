<?php

namespace App\Services;

use App\Entity\File;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LinkGeneratorService
{
    /** @var UrlGeneratorInterface */
    protected $urlGenerator;

    /**
     * LinkHelper constructor.
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function generateImageUrl(File $file = null, $size = 'original', $absolute = false)
    {
        if (null === $file) {
            return null;
        }

        $storagePath = trim($file->getStoragePath(), '/');
        $storagePath = str_replace('/', '-', $storagePath);

        $params = [
            'storagePath' => $storagePath,
            'hash' => $file->getHash(),
            'size' => $size,
            'ext' => $file->getExtension(),
        ];

        $referenceType = $absolute
            ? UrlGeneratorInterface::ABSOLUTE_URL
            : UrlGeneratorInterface::ABSOLUTE_PATH;

        return $this
            ->urlGenerator
            ->generate('tss.image', $params, $referenceType);
    }
}
