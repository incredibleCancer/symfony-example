<?php declare(strict_types=1);

namespace App\Forms;

use App\Entity\Client;
use App\Entity\Client\Source;
use App\Forms\Base\Base64FileType;
use App\Forms\Base\BooleanType;
use App\Forms\Client\PersonalContactsType;
use App\Forms\Client\WorkContactsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ClientType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 100]),
                ],
            ])
            ->add('lastname', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 100]),
                ],
            ])
            ->add('patronymic', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 100]),
                ],
            ])
            ->add('surname_in_latin', TextType::class, [
                'property_path' => 'lastnameInLatin',
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 100]),
                ],
            ])
            ->add('name_in_latin', TextType::class, [
                'property_path' => 'firstnameInLatin',
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 100]),
                ],
            ])
            ->add('sex', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('birthday', DateTimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text'
            ])
            ->add('nationality', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 50]),
                ],
            ])
            ->add('marital_status', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('personal_contacts', PersonalContactsType::class, [
                'property_path' => 'personalContacts',
            ])
            ->add('work_contacts', WorkContactsType::class, [
                'property_path' => 'workContacts',
            ])
            ->add('comment', TextType::class, [
                'constraints' => [
                ],
            ])
            ->add('active', BooleanType::class, [
                'constraints' => [
                ],
            ])
            ->add('source_id', EntityType::class, [
                'property_path' => 'source',
                'class' => Source::class,
            ])
            ->add('file_to_upload', Base64FileType::class, [
                'mapped' => false,
                'required' => false,
                'mime_types_extensions' => [
                    'image/jpeg' => 'jpg',
                    'image/jpg' => 'jpg',
                    'image/png' => 'png',
                ],
            ]);
    }

    /** @inheritdoc */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Client::class,
            'empty_data' => new Client(),
            'allow_extra_fields' => true,
        ]);
    }
}
