<?php declare(strict_types=1);

namespace App\Forms\Client;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class WorkContactsType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', TextType::class, [
                'constraints' => [
                    new Length(['max' => 150]),
                ],
            ])
            ->add('position', TextType::class, [
                'constraints' => [
                    new Length(['max' => 50]),
                ],
            ])
            ->add('address', TextType::class, [
                'constraints' => [
                    new Length(['max' => 255]),
                ],
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new Length(['max' => 50]),
                ],
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new Length(['max' => 100]),
                ],
            ])
            ->add('additional', TextType::class, [
                'constraints' => [
                ],
            ]);
    }

    /** @inheritdoc */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Client\WorkContacts::class,
            'empty_data' => new Client\WorkContacts(),
            'allow_extra_fields' => true,
        ]);
    }
}
