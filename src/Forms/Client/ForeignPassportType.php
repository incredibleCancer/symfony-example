<?php declare(strict_types=1);

namespace App\Forms\Client;

use App\Entity\Client;
use App\Entity\Client\ForeignPassport;
use App\Forms\Base\Base64FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ForeignPassportType extends AbstractType
{
    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client_id', EntityType::class, [
                'property_path' => 'client',
                'class' => Client::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('number', TextType::class, [
                'constraints' => [
                    new Length(['max' => 150]),
                    new NotBlank()
                ],
            ])
            ->add('issued_by', TextType::class, [
                'constraints' => [
                    new Length(['max' => 150]),
                    new NotBlank()
                ],
            ])
            ->add('issued_at', DateTimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text'
            ])
            ->add('finish_at', DateTimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text'
            ])
            ->add('file', Base64FileType::class, [
                'mapped' => false,
                'required' => false,
                'mime_types_extensions' => [
                    'image/jpeg' => 'jpg',
                    'image/jpg' => 'jpg',
                    'image/png' => 'png',
                ],
            ]);
    }

    /** @inheritdoc */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => ForeignPassport::class,
            'empty_data' => new ForeignPassport(),
            'allow_extra_fields' => true,
            'constraints' => [
                new UniqueEntity(['fields' => 'id']),
            ],
        ]);
    }
}
