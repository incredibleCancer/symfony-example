<?php declare(strict_types=1);

namespace App\Forms;

use App\Entity\Country;
use App\Entity\Partner;
use App\Forms\Base\Base64FileType;
use App\Services\CountryService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class PartnerType extends AbstractType
{
    /** @var CountryService */
    protected $countryService;

    /**
     * PartnerType constructor.
     * @param CountryService $countryService
     */
    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    /** @inheritdoc */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 255]),
                ],
            ])
            ->add('contact_info', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 1024]),
                ],
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 50]),
                ],
            ])
            ->add('url', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 100]),
                    new Url()
                ],
            ])
            ->add('comment', TextType::class, [
                'constraints' => [
                    new Length(['max' => 64000]),
                ],
            ])
            ->add('country_ids', ChoiceType::class, [
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                'choice_loader' => new CallbackChoiceLoader(function() {
                    return $this->countryService->getAllCountries();
                }),
                'choice_value' => function (Country $entity = null) {
                    return $entity ? $entity->getId() : '';
                },
            ])
            ->add('file_to_upload', Base64FileType::class, [
                'mapped' => false,
                'required' => false,
                'mime_types_extensions' => [
                    'image/jpeg' => 'jpg',
                    'image/jpg' => 'jpg',
                    'image/png' => 'png',
                ],
            ]);
    }

    /** @inheritdoc */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => Partner::class,
            'empty_data' => new Partner(),
            'allow_extra_fields' => true,
        ]);
    }
}
