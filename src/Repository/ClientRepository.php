<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Client;
use App\Request\Filters\ClientFilter;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findByFilter(ClientFilter $filter)
    {
        $qb = $this->createQueryBuilder('c');

        return $qb
            ->getQuery()
            ->getResult();
    }
}
