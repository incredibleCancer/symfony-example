<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Partner;
use App\Request\Filters\PartnerFilter;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Partner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partner[]    findAll()
 * @method Partner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartnerRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Partner::class);
    }

    /**
     * @param PartnerFilter $filter
     *
     * @return Partner[]
     */
    public function findByFilter(PartnerFilter $filter): array
    {
        $qb = $this->createQueryBuilder('p');

        if (null !== $filter->getCountry()) {
            $qb ->select('p')
                ->join('p.countries', 'c')
                ->andWhere('c.id = :country_id')
                ->setParameter('country_id', $filter->getCountry()->getId());
        }

        return $qb
            ->getQuery()
            ->getResult();
    }
}
