<?php declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class BaseRepository extends ServiceEntityRepository
{
    /**
     * @param \object $entity
     *
     * @return BaseRepository
     * @throws \Doctrine\ORM\ORMException
     */
    public function persist(object $entity): self
    {
        $this->getEntityManager()
            ->persist($entity);

        return $this;
    }

    /**
     * @param \object $entity
     *
     * @return BaseRepository
     * @throws \Doctrine\ORM\ORMException
     */
    public function flush(object $entity): self
    {
        $this->getEntityManager()
            ->flush($entity);

        return $this;
    }

    /**
     * @param \object $entity
     *
     * @return BaseRepository
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(object $entity): self
    {
        $this->persist($entity)
            ->flush($entity);

        return $this;
    }

    /**
     * @param \object $entity
     *
     * @return BaseRepository
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(object $entity): self
    {
        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush($entity);

        return $this;
    }
}
