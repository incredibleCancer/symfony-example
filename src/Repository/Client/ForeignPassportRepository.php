<?php declare(strict_types=1);

namespace App\Repository\Client;

use App\Entity\Client\ForeignPassport;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ForeignPassport|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForeignPassport|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForeignPassport[]    findAll()
 * @method ForeignPassport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForeignPassportRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForeignPassport::class);
    }
}
