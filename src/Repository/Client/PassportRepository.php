<?php declare(strict_types=1);

namespace App\Repository\Client;

use App\Entity\Client\Passport;
use App\Repository\BaseRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Passport|null find($id, $lockMode = null, $lockVersion = null)
 * @method Passport|null findOneBy(array $criteria, array $orderBy = null)
 * @method Passport[]    findAll()
 * @method Passport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PassportRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Passport::class);
    }
}
