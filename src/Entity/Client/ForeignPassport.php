<?php declare(strict_types=1);

namespace App\Entity\Client;

use App\Entity\Client;
use App\Entity\File;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Client\ForeignPassportRepository")
 * @ORM\Table(name="client_foreign_passport")
 */
class ForeignPassport
{
    const FIELD_CLIENT = 'Client.ForeignPassport.client';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $number;

    /**
     * @var string|null
     *
     * @ORM\Column(name="issued", type="string", length=100, nullable=true)
     */
    protected $issuedBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="issuedAt", type="date", nullable=true)
     */
    protected $issuedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="finishAt", type="date", nullable=true)
     */
    protected $finishAt;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="foreignPassports", fetch="LAZY")
     * @ORM\JoinColumn(name="clientId", referencedColumnName="id", onDelete="RESTRICT")
     */
    protected $client;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\File", fetch="EAGER")
     * @ORM\JoinColumn(name="scanId", referencedColumnName="id", onDelete="RESTRICT")
     */
    protected $scan;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getIssuedBy(): ?string
    {
        return $this->issuedBy;
    }

    public function setIssuedBy(?string $issuedBy): self
    {
        $this->issuedBy = $issuedBy;

        return $this;
    }

    public function getIssuedAt(): ?\DateTime
    {
        return $this->issuedAt;
    }

    public function setIssuedAt(?\DateTime $issuedAt): self
    {
        $this->issuedAt = $issuedAt;

        return $this;
    }

    public function getFinishAt(): ?\DateTime
    {
        return $this->finishAt;
    }

    public function setFinishAt(?\DateTime $finishAt): self
    {
        $this->finishAt = $finishAt;

        return $this;
    }

    public function isActive(): bool {
        if (null === $this->getFinishAt()) {
            return false;
        }

        $isActive = $this->getFinishAt() > (new \DateTime());

        return $isActive;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getClientId(): ?int
    {
        if (null === $this->getClient()) {
            return null;
        }

        return $this->getClient()->getId();
    }

    public function getScan(): ?File
    {
        return $this->scan;
    }

    public function setScan(File $scan): self
    {
        $this->scan = $scan;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
