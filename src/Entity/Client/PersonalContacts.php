<?php declare(strict_types=1);

namespace App\Entity\Client;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class PersonalContacts
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $mobile;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="homeAddress", type="string", length=255)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="homePhone", type="string", length=50)
     */
    protected $homePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="additionalContacts", type="text")
     */
    protected $additional;

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSkype(): ?string
    {
        return $this->skype;
    }

    public function setSkype(?string $skype): self
    {
        $this->skype = $skype;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getHomePhone(): ?string
    {
        return $this->homePhone;
    }

    public function setHomePhone(?string $homePhone): self
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    public function getAdditional(): ?string
    {
        return $this->additional;
    }

    public function setAdditional(?string $additional): self
    {
        $this->additional = $additional;

        return $this;
    }
}
