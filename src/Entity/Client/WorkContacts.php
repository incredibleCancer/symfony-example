<?php declare(strict_types=1);

namespace App\Entity\Client;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class WorkContacts
{
    /**
     * @var string
     *
     * @ORM\Column(name="companyName", type="string", length=150)
     */
    protected $company;

    /**
     * @var string
     *
     * @ORM\Column(name="jobPosition", type="string", length=50)
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="workAddress", type="string", length=255)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="workPhone", type="string", length=50)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="workEmail", type="string", length=100)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="workAdditionalContacts", type="text")
     */
    protected $additional;

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(?string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdditional(): ?string
    {
        return $this->additional;
    }

    public function setAdditional(?string $additional): self
    {
        $this->additional = $additional;

        return $this;
    }
}
