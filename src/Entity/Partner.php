<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartnerRepository")
 * @ORM\Table(name="partner")
 */
class Partner
{
    const FIELD_FILE = 'Partner.file';
    const FIELD_COUNTRIES = 'Partner.countries';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="contactInfo", type="string", length=1024)
     */
    protected $contactInfo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $comment;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\File", fetch="EAGER")
     * @ORM\JoinColumn(name="fileId", referencedColumnName="id", nullable=true, onDelete="RESTRICT")
     */
    protected $file;

    /**
     * @var Collection|Country[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Country")
     * @ORM\JoinTable(
     *     name="partner_country",
     *     joinColumns={@ORM\JoinColumn(name="partnerId", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="countryId", referencedColumnName="id")}
     * )
     */
    protected $countries;

    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContactInfo(): ?string
    {
        return $this->contactInfo;
    }

    public function setContactInfo(string $contactInfo): self
    {
        $this->contactInfo = $contactInfo;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function getFileId(): ?int
    {
        if (null === $this->getFile()) {
            return null;
        }

        return $this->getFile()->getId();
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return Collection|Country[]
     */
    public function getCountries(): Collection
    {
        return $this->countries;
    }

    /**
     * @param Collection|Country[] $countries
     *
     * @return $this
     */
    public function setCountries($countries): self
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * @return int[]
     */
    public function getCountryIds(): array
    {
        $ids = [];

        foreach ($this->getCountries() as $country) {
            $ids[] = $country->getId();
        }

        return $ids;
    }
}
