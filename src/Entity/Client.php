<?php declare(strict_types=1);

namespace App\Entity;

use App\Entity\Client\Attachment;
use App\Entity\Client\ForeignPassport;
use App\Entity\Client\Passport;
use App\Entity\Client\Payment;
use App\Entity\Client\Relation;
use App\Entity\Client\Source;
use App\Entity\Client\PersonalContacts;
use App\Entity\Client\WorkContacts;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 * @ORM\Table(name="client")
 */
class Client
{
    const FIELD_SOURCE = 'Client.source';
    const FIELD_PHOTO = 'Client.photo';
    const FIELD_ATTACHMENTS = 'Client.attachments';
    const FIELD_PASSPORTS = 'Client.passports';
    const FIELD_FOREIGN_PASSPORTS = 'Client.foreignPassports';
    const FIELD_PHOTOS = 'Client.photos';
    const FIELD_RELATIONS = 'Client.relations';
    const FIELD_RELATED_CLIENTS = 'Client.relatedClients';
    const FIELD_RELATED_CLIENT_IDS = 'Client.relatedClientIds';
    const FIELD_PAYMENTS = 'Client.payments';
    const FIELD_REQUESTS = 'Client.requests';
    const FIELD_REQUEST_IDS = 'Client.requestIds';

    const SEX_MALE = 'M';
    const SEX_FEMALE = 'F';

    const MARITAL_STATUS_SINGLE = 'single';
    const MARITAL_STATUS_MARRIED = 'married';
    const MARITAL_STATUS_DIVORCED = 'divorced';
    const MARITAL_STATUS_WIDOWER = 'widower';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Source|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Client\Source", fetch="EAGER", inversedBy="clients")
     * @ORM\JoinColumn(name="sourceId", referencedColumnName="id", nullable=true)
     */
    protected $source;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=100)
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $patronymic;

    /**
     * @var string
     *
     * @ORM\Column(name="nameInLatin", type="string", length=100)
     */
    protected $firstnameInLatin;

    /**
     * @var string
     *
     * @ORM\Column(name="surnameInLatin", type="string", length=100)
     */
    protected $lastnameInLatin;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, columnDefinition="ENUM('M', 'F')")
     */
    protected $sex;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $birthday;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    protected $nationality;

    /**
     * @var string
     *
     * @ORM\Column(name="maritalStatus", type="string", length=8, columnDefinition="ENUM('single','married','divorced','widower')")
     */
    protected $maritalStatus;

    /**
     * @var Collection|Payment[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Client\Payment", fetch="EXTRA_LAZY", mappedBy="client")
     */
    protected $payments;

    /**
     * @var File|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\File", fetch="EAGER")
     * @ORM\JoinColumn(name="photoId", referencedColumnName="id", nullable=true, onDelete="RESTRICT")
     */
    protected $photo;

    /**
     * @var Collection|Attachment[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Client\Attachment", fetch="EXTRA_LAZY", mappedBy="client")
     */
    protected $attachments;

    /**
     * @var Collection|Request[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Request", fetch="EXTRA_LAZY", mappedBy="client")
     */
    protected $requests;

    /**
     * @var Collection|Passport[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Client\Passport", fetch="EXTRA_LAZY", mappedBy="client")
     */
    protected $passports;

    /**
     * @var Collection|ForeignPassport[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Client\ForeignPassport", fetch="EXTRA_LAZY", mappedBy="client")
     */
    protected $foreignPassports;

    /**
     * @var Collection|File[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\File", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="client_photo",
     *     joinColumns={@ORM\JoinColumn(name="clientId", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="fileId", referencedColumnName="id")}
     * )
     */
    protected $photos;

    /**
     * @var Collection|Relation[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Client\Relation", fetch="EXTRA_LAZY", mappedBy="client")
     */
    protected $relations;

    /**
     * @var Collection|Relation[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Client\Relation", fetch="EXTRA_LAZY", mappedBy="relatedClient")
     */
    protected $backRelations;

    /**
     * @var PersonalContacts
     *
     * @ORM\Embedded(class="App\Entity\Client\PersonalContacts", columnPrefix=false)
     */
    protected $personalContacts;

    /**
     * @var WorkContacts
     *
     * @ORM\Embedded(class="App\Entity\Client\WorkContacts", columnPrefix=false)
     */
    protected $workContacts;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    protected $active = true;

//    protected $ekCard;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->personalContacts = new PersonalContacts();
        $this->workContacts = new WorkContacts();
        $this->attachments = new ArrayCollection();
        $this->passports = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->relations = new ArrayCollection();
        $this->backRelations = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->requests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function getSourceId(): ?int
    {
        if (null === $this->getSource()) {
            return null;
        }

        return $this->getSource()->getId();
    }

    public function setSource(?Source $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function setPatronymic(?string $patronymic): self
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    public function getFirstnameInLatin(): ?string
    {
        return $this->firstnameInLatin;
    }

    public function setFirstnameInLatin(?string $firstnameInLatin): self
    {
        $this->firstnameInLatin = $firstnameInLatin;

        return $this;
    }

    public function getLastnameInLatin(): ?string
    {
        return $this->lastnameInLatin;
    }

    public function setLastnameInLatin(?string $lastnameInLatin): self
    {
        $this->lastnameInLatin = $lastnameInLatin;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(?string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTime $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getMaritalStatus(): ?string
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(?string $maritalStatus): self
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    public function getPhoto(): ?File
    {
        return $this->photo;
    }

    public function getPhotoId(): ?int
    {
        if (null === $this->getPhoto()) {
            return null;
        }

        return $this->getPhoto()->getId();
    }

    public function setPhoto(?File $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getPersonalContacts(): PersonalContacts
    {
        return $this->personalContacts;
    }

    public function setPersonalContacts(PersonalContacts $personalContacts): self
    {
        $this->personalContacts = $personalContacts;

        return $this;
    }

    public function getWorkContacts(): WorkContacts
    {
        return $this->workContacts;
    }

    public function setWorkContacts(WorkContacts $workContacts): self
    {
        $this->workContacts = $workContacts;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Attachment[]|Collection
     */
    public function getAttachments(): ?Collection
    {
        return $this->attachments;
    }

    /**
     * @param Attachment[]|Collection $attachments
     *
     * @return $this
     */
    public function setAttachments(Collection $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * @return Passport[]|Collection
     */
    public function getPassports(): ?Collection
    {
        return $this->passports;
    }

    /**
     * @return Passport|null
     */
    public function getPassport(): ?Passport
    {
        if (count($this->passports)) {
            return $this->passports[0];
        }

        return null;
    }

    /**
     * @param Passport[]|Collection $passports
     *
     * @return $this
     */
    public function setPassports(Collection $passports): self
    {
        $this->passports = $passports;

        return $this;
    }

    /**
     * @return ForeignPassport[]|Collection
     */
    public function getForeignPassports(): ?Collection
    {
        return $this->foreignPassports;
    }

    /**
     * @return ForeignPassport|null
     */
    public function getForeignPassport(): ?ForeignPassport
    {
        if (count($this->foreignPassports)) {
            return $this->foreignPassports[0];
        }

        return null;
    }

    /**
     * @param ForeignPassport[]|Collection $passports
     *
     * @return $this
     */
    public function setForeignPassports(Collection $passports): self
    {
        $this->foreignPassports = $passports;

        return $this;
    }

    /**
     * @return File[]|Collection
     */
    public function getPhotos(): ?Collection
    {
        return $this->photos;
    }

    /**
     * @param File[]|Collection $photos
     *
     * @return $this
     */
    public function setPhotos(Collection $photos): self
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * @return Payment[]|Collection
     */
    public function getPayments(): ?Collection
    {
        return $this->payments;
    }

    /**
     * @param Payment[]|Collection $payments
     *
     * @return $this
     */
    public function setPayments(Collection $payments): self
    {
        $this->payments = $payments;

        return $this;
    }

    /**
     * @return Request[]|Collection
     */
    public function getRequests(): ?Collection
    {
        return $this->requests;
    }

    /**
     * @param Request[]|Collection $requests
     *
     * @return $this
     */
    public function setRequests(Collection $requests): self
    {
        $this->requests = $requests;

        return $this;
    }

    /**
     * @return int[]
     */
    public function getRequestIds(): ?array
    {
        $ids = [];

        foreach ($this->getRequests() as $request) {
            $ids[] = $request->getId();
        }

        return $ids;
    }

    /**
     * @return Relation[]|Collection
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @param Relation[]|Collection $relations
     *
     * @return Client
     */
    public function setRelations(Collection $relations): self
    {
        $this->relations = $relations;

        return $this;
    }

    /**
     * @return Relation[]|Collection
     */
    public function getBackRelations()
    {
        return $this->backRelations;
    }

    /**
     * @param Relation[]|Collection $relations
     *
     * @return Client
     */
    public function setBackRelations(Collection $relations): self
    {
        $this->backRelations = $relations;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getRelatedClients(): ?Collection
    {
        $clients = new ArrayCollection();

        foreach ($this->relations as $relation) {
            $clients->add($relation->getRelatedClient());
        }
        foreach ($this->backRelations as $relation) {
            $clients->add($relation->getClient());
        }

        return $clients;
    }

    /**
     * @return int[]
     */
    public function getRelatedClientIds(): ?array
    {
        $ids = [];

        foreach ($this->getRelatedClients() as $client) {
            $ids[] = $client->getId();
        }

        return $ids;
    }
}
